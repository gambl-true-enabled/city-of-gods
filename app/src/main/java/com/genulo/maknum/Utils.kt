package com.genulo.maknum

import android.content.Context
import android.webkit.JavascriptInterface

private const val GOLD_TABLE = "com.GOLD.table.123"
private const val GOLD_ARGS = "com.GOLD.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(GOLD_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(GOLD_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(GOLD_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(GOLD_ARGS, null)
}
